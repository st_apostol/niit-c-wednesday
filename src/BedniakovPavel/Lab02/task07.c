/*Symbol matrix*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int i=0, j=32, num=0, arr_num, count=0;
	char symbol[127], buf[200];
	puts("Enter a line:");
	fgets(buf, 199, stdin);
	while(j<127)
    {
		symbol[i]=j;
		arr_num=0;
		num=0;
		while(arr_num<=strlen(buf))
        {
			if(buf[arr_num]==j)
				num++;
			arr_num++;
			continue;
		}
        if(num!=00)
            printf("%c = %d\n", symbol[i], num);
		i++;
		j++;
	}
	putchar('\n');
	return 0;
}