/*Pyramid*/

#include <stdio.h>
#include <string.h>

int main()
{
    int lines, x, spc=0, count=1;
    puts("Enter amount of lines: ");
    scanf_s("%d", &lines);
    while(lines!=0)
    {
        spc=lines-1;
        for(;spc!=0;spc--)
            putchar(' ');
        x=count*2-1;
        for(;x!=0;x--)
            putchar('*');
        putchar('\n');
        lines--;
        count++;
    }
    return 0;
}