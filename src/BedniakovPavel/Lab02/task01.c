/*Bomb timer*/

#include <stdio.h>
#include <windows.h>

int main()
{
    int time=00;
    float L,height;
    printf("Put the height of bomb drop: ");
    scanf_s("%f",&height);
    while (height>0)
    {
        L=(9.81*(time*time))/2;
        height=height-L;
        if(height<0)
            break;
        printf("t=%d",time);
        printf("sec, h=%.2fm\n\a",height);       
        time++;
        Sleep(1000);
    }
    printf("\n\nKABOOM!!!\n\n");
    return 0;
}