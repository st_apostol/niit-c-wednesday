/*Password generator*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    int value;
    clock_t now;
    int count=0, x=0;
    srand(time(0));
    while(x!=10)
    {
        count++;
        value=rand()%3+1;
        switch(value)
        {
        case 1:
            value=rand()%('Z'-'A'+1)+'A';
            break;
        case 2:
            value=rand()%('z'-'a'+1)+'a';
            break;
        case 3:
            value=rand()%('9'-'0'+1)+'0';
        }
        putchar(value);
        if(count%8==0)
            putchar('\n'), x++;
        now=clock();
        while(clock()<now+10);
    }
    return 0;
}