#include<stdio.h>
#include<string.h>
#define IN 1
#define OUT 0

int main()
{
	char str[80];
	int count = 0;        // count words
	int status = OUT;     // flag inWord or outWord
	int i = 0;            // for loop while
	int start, end, st;   // for loop print

	puts("Enter you string: ");
	fgets(str, 80, stdin);
	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = 0;

	while (str[i])
	{
		if (str[i] != ' ' && status == OUT)
		{
			count++;
			status = IN;   // flag changed
			start = i;     // save value i
		}
		else if (str[i] == ' ' && status == IN)
		{
			end = i;								// save value i
			for (st = start; st < end; st++)
				printf("%c", str[st]);
			printf("\tlen=%d\n", (end - start));
			status = 0;
		}
		i++;
	}
	if (status == IN)                // end condition
		end = i;
	for (st = start; st < end; st++)
		printf("%c", str[st]);
	printf("\tlen=%d\n", (end - start));
	printf("Total: %d word(s)\n", count);

	return 0;
}