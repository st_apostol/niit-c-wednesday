#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h> 
#include <stdlib.h>   
#define N 10			// size array
#define MAX 100			// range

int main()
{
	int arr[N];
	int i = 0, j, start, end;							// for loop
	int sum = 0, num;
	int positive = 0, negative = 0;						// count positive and negative number
	srand(time(0));
	while (i < N)
	{
		num = rand() % MAX + 1 - (MAX / 2);			// numbers from -50 to + 50
		if ((num < 0) && (negative < N / 2)) {
			arr[i] = num;
			negative++;
			i++;
		}
		if ((num >= 0) && (positive < N / 2)) {
			arr[i] = num;
			positive++;
			i++;
		}
	}
	puts("Your the array: ");
	for (i = 0; i < N; i++)
		printf("%d ", arr[i]);
	putchar('\n');
	// sort array 
	for (i = 0; i < N-1; i++){
		for (j = 0; j < N - 1 - i; j++){
			if (arr[j]>arr[j+1]){
				num = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = num;
			}
		}
	}
	for (i = 0; i < N; i++)
		printf("%d ", arr[i]);
	putchar('\n');
	printf("Summa max and min figures = %d!\n", arr[0] + arr[N - 1]);
	return 0;
}