#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define SIZE 80
#define IN 1
#define OUT 0

void buffer();
int main()
{
	char str[SIZE];
	int i=0;					   //for loop
	int status = OUT;			   // flag
	int count = 0;                 // count word
	int start, end, num, ch;	   // for word 

	puts("Enter a line: ");
	fgets(str, SIZE, stdin);
	if (str[strlen(str) - 1] == '\n')
		str[strlen(str) - 1] = 0;
	while (1)
	{
		puts("Enter a number: ");
		if (scanf("%d", &num) == 1)
			break;
		else
			puts("Input Error! Enter a number again: ");
		buffer();
	}	
	while (str[i])
	{
		if (str[i] != ' ' && status == OUT)
		{
			count++;
			status = IN;
			if (count == num)
				start = i;
		}
		else if ((str[i] == ' ' || str[i+1] == 0) && status == IN){
			status = OUT;
			if (count == num){
				end = i;
				break;
			}
		}
		i++;
	}
	if (num > count){
		puts("Input Error! Your number out of range.\n");
		printf("The number of words = %d\n", count);
		return 0;
	}
	puts("Your word below");
	for (i = start; i <= end; i++)
		printf("%c", str[i]);
	putchar('\n');
	return 0;
}
// clean buffer
void buffer()
{
	char ch;
	do
		ch = getchar();
	while (ch != '\n' && ch != EOF);
}




