#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define SIZE 30
#define PI 3.14159265358979323846

int main()
{
	char name[30];
	float angle, radian, degree, ch;
	char symbol;
	char mes[] = { "Enter, please" };

	printf("Hello! What is your name?\n%s:\n", mes);
	fgets(name, SIZE, stdin);
	name[strlen(name) - 1] = '\0';
	puts("Our programm convert from degrees to radians and from radians to degrees!\n");
	printf("%s value of angle in format 45.00D or 45.00R:", mes);

	while (1)
	{
		if (scanf("%f%c", &angle, &symbol) == 2)
		{
			switch (toupper(symbol)) 
			{
			case 'D':
				(float)radian = (float)angle*(180.0 / PI);
				printf("After convert we get the value %.4fR\n", radian);
				printf("You got %.4fR, %s, from %.2f%c! See you later.\n", radian, name, angle, symbol);
				return 0;

			case 'R':
				(float)degree = (float)angle*(PI / 180.0);
				printf("After convert we get the value %.4fD\n", degree);
				printf("You got %.4fD, %s, from %.2f%c! See you later.\n", degree, name, angle, symbol);
				return 0;
			}
		}
		else
			printf("Input Error! %s again:", mes);
		do
			ch = getchar();
		while (ch != '\n' && ch != EOF);
	}
}