#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define SIZE 30
#define ONEFOOT 12*2.54
#define ONEINCH 2.54

void buffer();

int main()
{
	char name[SIZE];
	char mes[] = { "Enter, please" };
	int inch = 0, foot = 0;
	float cm = 0;

	puts("Hello, you are welcome! Our program convert from inch and foot to cm.\n");
	puts("What is your name? Enter below:\n");
	fgets(name, SIZE, stdin);
	name[strlen(name) - 1] = '\0';
	printf("%s, How many have you full foot? %s:", name, mes); 
	while (1)
	{
		if (scanf("%d", &foot) == 1)
		{
			printf("%s, How many have you full inches? %s:", name, mes);    //toupper(name)? �� ������������ �� ���������� ���������������.
			while (1)
			{
				if (scanf("%d", &inch) == 1)
				{
					(float)cm = ONEFOOT*(float)foot + ONEINCH*(float)inch;
					printf("Your height is %.2fcm\n", cm);
					return 0;
				}
				// bufer for inches.
				else
					printf("Input Error! %s again:", mes);
				buffer();
			}
		}
		// bufer for foot
		else
			printf("Input Error! %s again:", mes);
		buffer();
	}
}
// clean buffer
void buffer()
{
	char ch;
	do
		ch = getchar();
	while (ch != '\n' && ch != EOF);
}
