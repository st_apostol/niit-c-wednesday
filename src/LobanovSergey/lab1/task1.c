#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define SIZE 30
#define SEX 10
#define ONEFOOT 12*2.54
#define ONEINCH 2.54

void buffer();
int main()
{
	char name[SIZE];
	char sex[SEX];
	char advice[][65] = {"you should to eat more", "you all right", "you are overweight. You should do exersize and eat good food"};
	char mes[][40] = {"Enter, please", "Input Error? Enter, please again"};
	char answer[][60] = { "Thank you handsome", "We are pleased to.\nWe will help you", "How old are you?"};
	char old[][60] = { "You are a baby", "How is it going, teenager", "How are your work",
					"You are the youngest of our party", "You look perfect today" };
	int age = 0, cm=0, choice;
	float weight = 0, index=0;

	// ask user name
	puts("Hello, my dear friend!\n");
	printf("What is your name? %s:\n", mes[0]);
	fgets(name, SIZE, stdin);
	name[strlen(name) - 1] = '\0';
	printf("Nice to meet you, %s!\n", name);
	// ask sex
	puts("Could you tell your sex? male or female?\n");
	fgets(sex, SEX, stdin);
	sex[strlen(sex) - 1] = '\0';
	printf("Your sex is %s!\n", sex);

	switch (sex[0])
	{    // branch man
		case 'm': case 'M':
			printf("%s, %s!\n", answer[0], name);
			printf("%s, Mr %s!\n", answer[1], name);
			printf("%s %s:\n", answer[2], mes[0]);
			break;
		//branch woman
		case 'f': case 'F':
			printf("Thank you pretty, %s!\n", name);
			printf("%s, Mrs %s!\n", answer[1], name);
			printf("Excuse me, but i can ask you. %s %s:\n", answer[2], mes[0]);
			break;
	}	
	while (1)   // ask age
	{
		if ((scanf("%d", &age) == 1) && age>0 && age < 150)
		{
			if (age > 0 && age < 10){
				printf("%s, %s!=)\n", old[0], name);
				break;
			}
			else if (age >= 10 && age < 18){
				printf("%s %s? =^_^\n", old[1], name);
				break;
			}
			else if (age >= 18 && age < 40){
				printf("%s, %s?\n", old[2], name);
				break;
			}
			else if (age >= 40 && age < 70){
				printf("%s, %s?\n", old[3], name);
				break;
			}
			else if (age >= 70 && age < 150){
				printf("%s, %s!\n", old[4], name);
				break;
			}
		}		// close if
		else
			printf("%s\n", mes[1]);
		buffer();
	}			// close while
	// ask height
	printf("You know your height? %s (format: 180 cm):\n", mes[0]);
	while (1)
	{
		if ((scanf("%d", &cm) == 1)&&cm>0 && cm<300){
			printf("Okey! Your height is %d cm\n", cm);
			break;
		}
		else
			printf("%s\n", mes[1]);
		buffer();
	}
	// ask weight
	printf("You know your weight? %s (format 75.5 kg):\n", mes[0]);
	while (1)
	{
		if ((scanf("%f", &weight) == 1)&& weight>0 && weight<300){
			printf("Okey! Your weight is %.2f kg\n", weight);
			break;
		}
		else
			printf("%s\n", mes[1]);
		buffer();
	}
	printf("You %s is %s,\nYou have %d years old,\n%d cm your height and %.2f kg your weight.\n", name, sex, age, cm, weight);
	puts("\nWe will count your index Ketle.\n");
	(float)index = (float)weight / (float)((cm*cm) / 10000);         // count index Ketle
	if (index < 18.5)
		choice = 0;
	else if (index >= 18.5 && index < 25)
		choice = 1;
	else if (index > 25)
		choice = 2;
	// the end
	printf("Your index Ketle is %0.2f\n", index);
	printf("%s, %s. Good bay\n", name, advice[choice]);
	return 0;
}
// clean buffer
void buffer()
{
	char ch;
	do
		ch = getchar();
	while (ch != '\n' && ch != EOF);
}