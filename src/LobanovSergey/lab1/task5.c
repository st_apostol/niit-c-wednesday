#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define SIZE 80

int main()
{
	char buf[SIZE];
	char str[SIZE];     // stdin user
	int len = 0;
	puts("What do you think?\n");
	fgets(str, SIZE, stdin);
	len = (SIZE + strlen(str)) / 2;
	sprintf(buf, "%c%d%c", '%', len, 's');   // "% len s"
	printf(buf, str);

	return 0;
}
