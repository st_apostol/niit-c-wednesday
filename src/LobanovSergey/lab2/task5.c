#include <stdio.h>
#include <time.h> 
#include <stdlib.h>   // �������

int main()
{
	int value;
	int count = 0;		  // count value
	int newStr = 0;           // count '\n'
	srand(time(0));       
	
	while (1)
	{
		value = rand() % ('z' - '0' + 1) + '0';   // range 0-100
		
		if (value > '9' && value < 'A' || value > 'Z' && value < 'a')     // out our range
			continue;
		else
		{
			++count;
			putchar(value);
			if (count % 8 == 0){
				++newStr;
				putchar('\n');
				if (newStr == 10)
					break;
			}					// for if
		}						// for else		
	}							// for while

	printf("Now. You are dangerous!\n");
	return 0;
}