#include <stdio.h>
#include <string.h>
#define IN 1       // flag in word
#define OUT 0      // flag out word

int main()
{
	char str[] = "    For  the    world   you    may be     just one person, but for one    person you    may be the     whole     world   ";
	int status = OUT;
	int space = 0;        // count space
	int i;			      // for loop

	for (i = strlen(str) - 1; i >= 0; i--)        // go from the end
	{
		if (str[i] == ' ' && status == OUT && space == 0)
			str[i] = '\0';
		else if (str[i] != ' ' && status == OUT)
			status = IN;
		else if (str[i] == ' ' && status == IN){
			space = 1;
			status = OUT;
		}
		else if (str[i] == ' ' && space == 1 && status == OUT){
			for (i; i < strlen(str); i++)
				str[i] = str[i + 1];
		}	
	}
	if (str[0] == ' '){
		for (i=0; i < strlen(str); i++)
			str[i] = str[i + 1];
	}
	printf("%s\n", str);
	return 0;
}