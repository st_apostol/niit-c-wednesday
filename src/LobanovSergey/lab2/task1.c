#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#define G 9.81            // acceleration

int main()
{
	int t;            // time
	float h;          // height
	clock_t now;
	
	puts("Enter, please height:");
	scanf("%f", &h);

	for (t=0; (h - (G*t*t) / 2) > 0; t++)
	{
		printf("t = %3d  h = %6.2f\n", t, h - (G*t*t) / 2);
		now = clock();
		while(clock() < now + 1000);
	}
	printf("Nice to meet you\a\n");
	return 0;
}