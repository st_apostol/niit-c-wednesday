/*5. �������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define M 200

int main()
{
    FILE *fp,*out;
    int count=0,i=0,j=0;
    char str[M][255],*s[M],*tmp;
    fp=fopen("input.txt","rt");
    out=fopen("output.txt","wt");
    if (fp==0 || out==0)
    {
        perror("File");
        return 1;
    }
    while (fgets(str[i],255,fp))
    {
        s[i]=&str[i][0];
        i++;
    }
    count=i;
    for(i=0; i<count-1; i++)
    {            
        for(j=0; j<count-1; j++)
        {     
            if ((strlen(s[j+1]))<(strlen(s[j])))
            {
                tmp=s[j];
                s[j]=s[j+1];
                s[j+1]=tmp;
            }
        }
    }
    for (i=0; i<count; i++)
        fputs(s[i],out);
    fclose(fp);
    fclose(out);
    return 0;
}