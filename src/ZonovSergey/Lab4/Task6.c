/*6. �������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������*/

#include <stdio.h>
#include <string.h>
#define N 20
#define M 20

int main()
{
    char name[M][N],*young, *old;
    int i,age,n,maxage=0,minage=99;
    printf("Input the number of relatives: ");
    scanf("%d",&n);
    puts("Enter name and age:");
    for (i=0;i<n;i++)
    {
        scanf("%s%d",name[i],&age);
        if (age>maxage)
        {
            maxage=age;
            old=&name[i];
        }
        if (age<minage)
        {
            minage=age;
            young=&name[i];
        }
    }
    printf("The youngest is %s - %d\nThe oldest is %s - %d\n",young,minage,old,maxage);
    return 0;
}