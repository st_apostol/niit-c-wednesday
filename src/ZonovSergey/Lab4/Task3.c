/*3. �������� ���������, ������� ����������� ������ � ����������, �� ��������
�� ������ ����������� (��������� �������� � ����� ������� � ������ ������)
���������:
���� ������ - ��������� ��������� ��� �������� ������������ ������ � ����
������*/

#include <stdio.h>
#include <string.h>

int main()
{
    char str[80],*es,*bs;
    int count=0;
    puts("Enter a line:");
    fgets(str,80,stdin);
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]='\0';
    es=&str[strlen(str)-1];
    bs=&str[0];
    while(bs<=es)
    {
        if (*bs==*es)
        {
            *bs++;
            *es--;
            count++;
        }
        else
        {
            printf("Line is not palindrome\n");
            break;
        }
    }
    if (count>=((strlen(str)-1)/2))
        printf("Line is palindrome\n");
    return 0;
}
