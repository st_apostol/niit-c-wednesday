/*8. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� ������ ��
�����. � ������ ������������� n ��������� ��������� �� ������*/

#include <stdio.h>
#include <string.h>

int main ()
{
    char str[80];
    int i=0, count=0, n;
    int inWord=0;
    puts("Enter a line:");
    fgets(str,80,stdin);
    printf("Enter number of word: ");
    scanf("%d",&n);
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]='\0';

    while(str[i])
    {
        if (str[i]!=' ' && inWord==0)
        {
            count++;
            inWord=1;
        }
        else if (str[i]==' ' && inWord==1)
            inWord=0;
        if (n==count)
            printf("%c",str[i]);
        i++;
    }
    if ((n>count) || (n<=0))
        printf("You entered incorrect number.\n");
    else
        printf("\n");
    return 0;
}