/*10. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������
���������:
� ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int main ()
{
    char str[80];
    int i=0, count=0, n,j,l=0;
    int inWord=0,len;
    puts("Enter a line:");
    fgets(str,80,stdin);
    printf("Enter number of word: ");
    scanf("%d",&n);
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]='\0';
    len=strlen(str);

    while(str[i])
    {
        if (str[i]!=' ' && inWord==0)
        {
            count++;
            inWord=1;
        }
        else if (str[i]==' ' && inWord==1)
            inWord=0;
        if (n==count)
        {    
            j=i;
            l++;
        }
        i++;
    }
    if ((n>count) || (n<=0))
        printf("You entered incorrect number.\n");
    else
    {
        for ((i=j-l); i<=len; i++)
            str[i]=str[i+l];
        len=strlen(str);
        if (n==count)
            str[len-1]='\0';
        printf("%s\n",str);
    }
    return 0;
}