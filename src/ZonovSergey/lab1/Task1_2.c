/*2. �������� ���������, ������� ����������� ������� ����� � ���-
���� ��:��:��, � ����� ������� ����������� � ����������� ��
���������� ������� ("������ ���� "������ ����"� �.�.)*/

#include <stdio.h>

int main ()
{
    int h,m,s;
    char mess[][80]={"Good night","Good morning","Good day","Good evening"};
    printf("Enter current time please (hh:mm:ss): ");
    scanf("%d:%d:%d",&h,&m,&s);
    if ((h>=0) && (h<24) && (m>=0) && (m<60) && (s>=0) && (s<60))
        printf("%s!\n",mess[h<6?0:h<10?1:h<18?2:h<22?3:0]);
    else 
        printf("Input error!\n");

    return 0;
}