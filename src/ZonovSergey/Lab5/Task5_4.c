/*4. Написать программу, которая читает построчно текстовый файл и
переставляет случайно слова в каждой строке
Замечание:
Программа открывает существующий тектстовый файл и читает его построч-
но. Для каждой строки вызывается функция, разработанная в рамках задачи
1.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define M 255

//Разделение на слова
int getWords(char* str, char** s) 
{
	int j=0;
    s[j]=strtok(str," \n");
    while (s[j]!=NULL)
    {
        j++;
        s[j]=strtok(NULL," \n");
    }
	return j;
}

//Запись данных в файл
void printWord(FILE* out, char** s, int size, int n)
{
	if (n>=size || n<0) return;
	fprintf(out,"%s", s[n]);
}

//Перемешивание слов
void randomize(char**s, int size) 
{
	int ind1, ind2, k;
	char* tmp;
	srand(time(0));
	for (k=0; k<2*size; k++)
	{
		ind1=rand()%size;
		ind2=rand()%size;
		tmp=s[ind1];
		s[ind1]=s[ind2];
		s[ind2]=tmp;
	}
}

//Чтение входного файла, вызов функций разбивки, перемешивания, вывода 
void rwFile(FILE* fp, FILE* out)
{
    char str[M], *s[M];
	int i=0, j=0;
    while (fgets(str,255,fp))
    {
        if (str[0]=='\n')
            fprintf(out,"\n");
	    j=getWords(str, s);
    	randomize(s, j);
    	for (i=0; i<j; i++) 
    	{
    		printWord(out, s, j, i);
            (i==j-1)?(fprintf(out,"\n")):(fprintf(out," "));
    	}
    }
}

int main()
{
    FILE *fp,*out;
    fp=fopen("vm.txt","rt");
    out=fopen("output.txt","wt");
    if (fp==0 || out==0)
    {
        perror("File");
        return 1;
    }
    rwFile(fp,out);   
    fclose(fp);
    fclose(out);
	return 0;
}