/*2. Написать программу калейдоскоп, выводящую на экран изобра-
жение, составленное из симметрично расположенных звездочек Т*Т.
изображение формируется в двумерном символьном массиве, в од-
ной его части и симметрично копируется в остальные его части.
замечание:
Решение задачи протекает в виде следующей последовательности шагов:
1) Очистка массива (заполнение пробелами)
2) Формирование случайным образом верхнего левого квадранта (занесение "*")
3) Копирование символов в другие квадранты массива
4) Очистка экрана
5) Вывод массива на экран (построчно)
6) Временная задержка
7) Переход к шагу 1.*/

#include <stdlib.h>
#include <conio.h>
#include <stdio.h>
#include <time.h>
#include <windows.h>
#define M 71
#define N 70                                                    //размер матрицы, M>N для добавления символа окончания строки

void Quad(char (*scr)[M])                                       //заполнение первого квадранта "*" b и пробелами. копирование его в остальные квадранты.
{
    int x,y,k;
    srand(time(NULL));
    for (x=0;x<(M-1)/2;x++)
        for (y=0;y<(M-1)/2;y++)
		{
            (k=rand()%7)==0?(scr[x][y]='*'):(scr[x][y]=' ');
            scr[x][M-y-2]=scr[x][y];
            scr[M-x-2][y]=scr[x][y];
            scr[M-x-2][M-y-2]=scr[x][y];
		}
		
}

void PrSc(char (*scr)[M])                                       //случайный выбор цвета и построчный вывод
{
    HANDLE hStdOut=GetStdHandle(STD_OUTPUT_HANDLE);
    int x=0,col;
    srand(time(0));
    col=rand()%14+1;
    SetConsoleTextAttribute(hStdOut, col);
    for (x=0; x<(M-1);x++)
	{
		scr[x][M-1]='\0';
        printf("%s\n",scr[x]);
	}
}

int main()
{
    char stars[N][M];
    while (!kbhit())                                            //завершение цикла нажатием любой клавиши
    {
        Quad(stars);
        PrSc(stars);
        Sleep(900);
        system("cls");
        Sleep(100);
    }
    return 0;
}