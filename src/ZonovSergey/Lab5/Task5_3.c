/*3. �������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ����������� �������� �� ����� � ����������� ����-
����� ������� �����.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define M 255

//���������� �� �����
int getWords(char* str, char** s) 
{
	int j=0;
    s[j]=strtok(str," \n");
    while (s[j]!=NULL)
    {
        j++;
        s[j]=strtok(NULL," \n");
    }
	return j;
}

//������ ������ � ����
void printWord(FILE* out, char** s, int size, int n)
{
	if (n >= size || n < 0) return;
	fprintf(out,"%s", s[n]);
}

//������������� ���� � �����
void randomize(char**s, int w) 
{
	int ind1, ind2, size, k;
	char* tmp;
    size=strlen(s[w])-2;
	srand(time(NULL));
	for (k=0; k<2*size; k++)
	{
		ind1=(rand()%size)+1;
		ind2=(rand()%size)+1;
		tmp=s[w][ind1];
		s[w][ind1]=s[w][ind2];
		s[w][ind2]= tmp;
	}
}

//������ �������� �����, ����� ������� ��������, �������������, ������ 
void rwFile(FILE* fp, FILE* out)
{
    char str[M], *s[M];
	int i=0, j=0;
    while (fgets(str,255,fp))
    {
        if (str[0]=='\n')
            fprintf(out,"\n");
	    j=getWords(str, s);
    	for (i=0; i<j; i++) 
    	{
            randomize(s, i);
    		printWord(out, s, j, i);
            (i==j-1)?fprintf(out,"\n"):fprintf(out," ");
    	}
    }
}

int main()
{
    FILE *fp,*out;
    fp=fopen("vm.txt","rt");
    out=fopen("output.txt","wt");
    if (fp==0 || out==0)
    {
        perror("File");
        return 1;
    }
    rwFile(fp,out);   
    fclose(fp);
    fclose(out);
	return 0;
}