/*1. Написать программу, которая принимает от пользователя строку и
выводит ее на экран, перемешав слова в случайном порядке.
Замечание:
Программа должна состоять минимум из трех функций:
a) printWord - выводит слово из строки (до конца строки или пробела)
b) getWords - заполняет массив указателей адресами первых букв слов
c) main - основная функция*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define M 80

// Разделение строки по словам с помощью функции strtok
int getWords(char* str, char** s)
{
	int j=0;
    s[j]=strtok(str," ");
    while (s[j]!=NULL)
    {
        j++;
        s[j]=strtok(NULL," ");
    }
	return j;
}

//Вывод строки с перемешанными словами
void printWord(char** s, int size, int n)
{
	if (n>=size || n<0) return;
	printf("%s", s[n]);
}

//Перемешка слов
void randomize(char**s, int size) 
{
	int ind1, ind2,i;
	char* tmp;
	srand(time(NULL));
	for (i=0; i<2*size; i++)
	{
		ind1=rand()%size;
		ind2=rand()%size;
		tmp=s[ind1];
		s[ind1]=s[ind2];
		s[ind2]=tmp;
	}
}

int main()
{
	char str[M], *s[M];
	int i=0, j=0;
	puts("Enter a line:");
	fgets(str, M, stdin);
	if (str[strlen(str)-1]=='\n')
		str[strlen(str)-1]='\0';
	j=getWords(str, s);
	randomize(s, j);
	for (i=0; i<j; i++) 
	{
		printWord(s, j, i);
        (i==(j-1))?(printf("\n")):printf(" ");            
	}
	return 0;
}