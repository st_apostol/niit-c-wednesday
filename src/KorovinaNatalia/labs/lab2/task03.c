/*Task 3. Pyramid*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>


int main()
{
	int iRowsNumber, n, k;
	int count = 0;
	int iSpace = 0;
	int i= 1;
	
	char cStar = '*';
	char cSpace = ' ';
	char ch;

	while (1)
	{
			printf("Input the number of rows of pyramid:\n");
			if (scanf("%d", &iRowsNumber) == 1)
			{
				for (i = 0; i < iRowsNumber; i++)
				{
					for (n = 0; n < iRowsNumber - (i + 1); n++)
						printf("%c", cSpace);
					for (k = 0; k < 2 * i + 1;k++)
						printf("%c", cStar);
					printf("\n");

				}

				break;
			}
			else
			{
				puts("Input error!");
				do
					ch = getchar();
				while (ch != '\n'&& ch != EOF);

			}

	}
	return 0;

}
