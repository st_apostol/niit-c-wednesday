/*Lab03. Task09. �������� ���������, ������������ � ������ ���������� ������������������
������������ �����
��������� :
��� ������ AABCCCDDEEEEF ��������� 4 � EEEE*/


#include <stdio.h>
#include <string.h>

int main()
{
    char str[80], maxsymbol = 0;
    int i = 0, k = 0, count = 1, countmax = 0;

    puts("Enter a line:");
    fgets(str, 80, stdin);
    if (str[strlen(str) - 1] == '\n')
        str[strlen(str) - 1] = 0;

    while (str[i])
    {
        while (str[i] == str[i + 1])
        {
            count++;
            i++;
        }

        if (count > countmax)
        {
            countmax = count;
            maxsymbol = str[i];
        }
        count = 1;
        i++;
    }

    printf("Max string is: ");
    for (i = 0;i < countmax;i++)
        putchar(maxsymbol);
    printf(". The amount of symbols is: %d \n", countmax);

    return 0;
}