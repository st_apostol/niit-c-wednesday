/*Lab03. Task07. �������� ���������, ������� �������� ������� ������������� �������� ���
�������� ������, ��������������� �� �������� ������� */


#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ROW 80

int main()
{
    int i, j, tmp;
    char n;
    int m = 0;
    int z = 0;
    char cString[ROW] = { 0 };
    char cStatistic[128][3] = { 0 };

    puts("Input your row:");

    while ((cString[m] = getchar()) != '\n')				// ������ ��������� ������ � ������
    {
        m++;
    }

    for (i = 0; i < (strlen(cString) - 1); i++)           //������� ��������
    {
        n = cString[i];
        cStatistic[n][0] = cStatistic[n][0] + 1;

    }

    for (i = 0; i < 128; i++)  //���������� ������ �������� ��������
    {
        cStatistic[i][1] = i;
    }


    for (i = 0; i <127; ++i) // ���������� ������� ������� ��������
    {
        for (j = 0; j <127; ++j)
        {
            if (cStatistic[j + 1][0] > cStatistic[j][0])
            {
                tmp = cStatistic[j + 1][0];
                cStatistic[j + 1][0] = cStatistic[j][0];
                cStatistic[j][0] = tmp;

                tmp = cStatistic[j + 1][1];
                cStatistic[j + 1][1] = cStatistic[j][1];
                cStatistic[j][1] = tmp;
            }
        }
    }

    while (cStatistic[z][0] != 0)
    {
        if (cStatistic[z][1] == ' ')
            printf("Space	x%2d\n", cStatistic[z][0]);
        else
            printf("%5c	x%2d\n", cStatistic[z][1], cStatistic[z][0]);
        z++;
    }

    return 0;

}
