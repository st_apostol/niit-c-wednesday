/*Lab03. Task05. �������� ���������, ������� ��������� ������������� ������ ������� N, �
����� ������� ����� ���������, ������������� ����� ������ ����������-
��� � ��������� ������������� ����������*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define N 3


int main()
{
    int iOrder;
    int k, iMax = 0, iMin = 0, i = 0, iSum = 0;
    int count = 0;
    int cSymbol[N];
    srand(time(0));

    while (1)
    {
        k = 0;
        while (k < N)						//��������� �������
        {
            iOrder = rand() % 2 + 1;

            switch (iOrder)
            {
            case 1:
                cSymbol[k] = (rand() % 100 + 1);
                break;
            case 2:
                cSymbol[k] = -(rand() % 100 + 1);
                break;
            }

            printf("%d ", cSymbol[k]);

            k++;
        }

        while (i < N)						//����� ������� ������� �������������� �����
        {
            if (cSymbol[i] < 0)
            {
                iMin = i;
                i = N - 1;
                break;
            }
            i++;
        }

        while (i > 0)						//����� ������� ���������� �������������� ����� 
        {
            if (cSymbol[i] > 0)
            {
                iMax = i;
                break;
            }
            i--;
        }

        if (iMax <= iMin)						// ���� �� ����������� �������� ������� ������������ ����� ������������ ����� �����
        {
            printf("Can't use generated string. The new one will be generated.\n");
            continue;
        }
        else
            break;

    }


    printf("\n");
    for (i = iMin;i < iMax + 1;i++)						// ���������� ����� ����� �� ���������� ���������
        iSum = iSum + cSymbol[i];
    printf("The sum from %d to %d is %d\n", cSymbol[iMin], cSymbol[iMax], iSum);
    return 0;



}
